//const MongoClient = require('mongodb').MongoClient;

const { MongoClient, ObjectID } = require('mongodb');

MongoClient.connect('mongodb://localhost:27017/TodoApp', (err, db) => {

    if (err) {
        return console.log('Imposible conectarse con el Servidor de Mongo DB');
    }
    console.log('Conectado con el Servidor Mongo DB');

    //buscar array collection
    // db.collection('Todos').find({ completed: false }).toArray().then((docs) => {
    //     console.log('Todos');
    //     console.log('Documentos: ', JSON.stringify(docs, undefined, 2));
    // }, (err) => {
    //     console.log('Imposible encontrar el registros en Todos...');
    // });

    // db.collection('Todos').find({
    //     _id: new ObjectID('5c364e09654e1bd869c7fed6')
    // }).toArray().then((docs) => {
    //     console.log('Todos');
    //     console.log('Documentos: ', JSON.stringify(docs, undefined, 2));
    // }, (err) => {
    //     console.log('Imposible encontrar el registros en Todos...');
    // });

    // db.collection('Todos').find().count().then((count) => {
    //     console.log(`Todos count: ${ count }`);
    // }, (err) => {
    //     console.log('Imposible encontrar el registros en Todos...');
    // });

    //buscar todos los usarios cuyo nombre sea Leonardo Rozo
    db.collection('Users').find({
        name: 'Leonardo Rozo'
            //age: 8
            //age: { $gte: 30, $lte: 40 }

    }).toArray().then((docs) => {
        console.log('Documentos: ', JSON.stringify(docs, undefined, 2));
    }, (err) => {
        console.log('Imposible encontrar el registros en Todos...', err);
    });

    //cerrar la conexion con la bd test
    //db.close();
});