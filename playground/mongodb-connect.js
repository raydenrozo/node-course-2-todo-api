//const MongoClient = require('mongodb').MongoClient;

const { MongoClient, ObjectID } = require('mongodb');


// var obj = new ObjectID();

// console.log(obj);

//Ejemplo de destructuracion tomar en cuenta...
// var user = {
//     name: 'Leonardo',
//     age: 08
// };

// var { name } = user;
// console.log(name);

MongoClient.connect('mongodb://localhost:27017/TodoApp', (err, db) => {

    if (err) {
        return console.log('Imposible conectarse con el Servidor de Mongo DB');
    }
    console.log('Conectado con el Servidor Mongo DB');

    //crear nueva collection

    // db.collection('Todos').insertOne({
    //     text: 'Something to do',
    //     completed: false
    // }, (err, result) => {
    //     if (err) {
    //         return console.log('Imposible insertar el documento');
    //     }
    //     console.log('Documento registrado exitosamente: ', JSON.stringify(result.ops, undefined, 2));
    // })

    //crear nueva collection con desafio    
    // db.collection('Users').insertOne({
    //     name: 'Leonardo Rozo',
    //     age: 8,
    //     location: 'Santiago de Chile',
    //     completed: false
    // }, (err, result) => {
    //     if (err) {
    //         return console.log('Imposible insertar el documento');
    //     }
    //     console.log('Documento registrado exitosamente: ',
    //         JSON.stringify(result.ops[0]._id.getTimestamp(), undefined, 2));
    // })


    //cerrar la conexion con la bd test
    db.close();
})